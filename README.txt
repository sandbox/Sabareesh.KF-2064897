Readme file for the Password Change Tab-Block module for Drupal
------------------------------------------------------------------

At present in user/uid/edit page we have the option to change the password.
This at times will be confusing to end user about what it might deal with.

To avoid this confusion, this module moves the password change to a new tab.

Also a block has been provided.

This Block reduces the complexity to move between pages to change password.

Thus this project reduces the complexity involved in changing the password.

This module will be highly usefull to novice drupal users.
